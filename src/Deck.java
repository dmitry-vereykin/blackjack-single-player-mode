import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

/**
 * Created by Dmitry Vereykin on 12/15/2015.
 */
public class Deck {
    public Stack<Card> deck;
    public ArrayList<Card> sortedDeck;

    public Deck() {
        sortedDeck = new ArrayList<>();
        sortedDeck.add(new Card("two_Clubs", 2, "Cards/2_Clubs.jpg"));
        sortedDeck.add(new Card("two_Diamonds", 2, "Cards/2_Diamonds.jpg"));
        sortedDeck.add(new Card("two_Hearts", 2, "Cards/2_Hearts.jpg"));
        sortedDeck.add(new Card("two_Clubs", 2, "Cards/2_Clubs.jpg"));

        sortedDeck.add(new Card("three_Clubs", 3, "Cards/3_Clubs.jpg"));
        sortedDeck.add(new Card("three_Diamonds", 3, "Cards/3_Diamonds.jpg"));
        sortedDeck.add(new Card("three_Hearts", 3, "Cards/3_Hearts.jpg"));
        sortedDeck.add(new Card("three_Clubs", 3, "Cards/3_Clubs.jpg"));

        sortedDeck.add(new Card("four_Clubs", 4, "Cards/4_Clubs.jpg"));
        sortedDeck.add(new Card("four_Diamonds", 4, "Cards/4_Diamonds.jpg"));
        sortedDeck.add(new Card("four_Hearts", 4, "Cards/4_Hearts.jpg"));
        sortedDeck.add(new Card("four_Spades", 4, "Cards/4_Spades.jpg"));

        sortedDeck.add(new Card("five_Clubs", 5, "Cards/5_Clubs.jpg"));
        sortedDeck.add(new Card("five_Diamonds", 5, "Cards/5_Diamonds.jpg"));
        sortedDeck.add(new Card("five_Hearts", 5, "Cards/5_Hearts.jpg"));
        sortedDeck.add(new Card("five_Spades", 5, "Cards/5_Spades.jpg"));

        sortedDeck.add(new Card("six_Clubs", 6, "Cards/6_Clubs.jpg"));
        sortedDeck.add(new Card("six_Diamonds", 6, "Cards/6_Diamonds.jpg"));
        sortedDeck.add(new Card("six_Hearts", 6, "Cards/6_Hearts.jpg"));
        sortedDeck.add(new Card("six_Spades", 6, "Cards/6_Spades.jpg"));

        sortedDeck.add(new Card("seven_Clubs", 7, "Cards/7_Clubs.jpg"));
        sortedDeck.add(new Card("seven_Diamonds", 7, "Cards/7_Diamonds.jpg"));
        sortedDeck.add(new Card("seven_Hearts", 7, "Cards/7_Hearts.jpg"));
        sortedDeck.add(new Card("seven_Spades", 7, "Cards/7_Spades.jpg"));

        sortedDeck.add(new Card("eight_Clubs", 8, "Cards/8_Clubs.jpg"));
        sortedDeck.add(new Card("eight_Diamonds", 8, "Cards/8_Diamonds.jpg"));
        sortedDeck.add(new Card("eight_Hearts", 8, "Cards/8_Hearts.jpg"));
        sortedDeck.add(new Card("eight_Spades", 8, "Cards/8_Spades.jpg"));

        sortedDeck.add(new Card("nine_Clubs", 9, "Cards/9_Clubs.jpg"));
        sortedDeck.add(new Card("nine_Diamonds", 9, "Cards/9_Diamonds.jpg"));
        sortedDeck.add(new Card("nine_Hearts", 9, "Cards/9_Hearts.jpg"));
        sortedDeck.add(new Card("nine_Spades", 9, "Cards/9_Spades.jpg"));

        sortedDeck.add(new Card("ten_Clubs", 10, "Cards/10_Clubs.jpg"));
        sortedDeck.add(new Card("ten_Diamonds", 10, "Cards/10_Diamonds.jpg"));
        sortedDeck.add(new Card("ten_Hearts", 10, "Cards/10_Hearts.jpg"));
        sortedDeck.add(new Card("ten_Spades", 10, "Cards/10_Spades.jpg"));

        sortedDeck.add(new Card("Jack_Clubs", 10, "Cards/Jack_Clubs.jpg"));
        sortedDeck.add(new Card("Jack_Diamonds", 10, "Cards/Jack_Diamonds.jpg"));
        sortedDeck.add(new Card("Jack_Hearts", 10, "Cards/Jack_Hearts.jpg"));
        sortedDeck.add(new Card("Jack_Spades", 10, "Cards/Jack_Spades.jpg"));

        sortedDeck.add(new Card("Queen_Clubs", 10, "Cards/Queen_Clubs.jpg"));
        sortedDeck.add(new Card("Queen_Diamonds", 10, "Cards/Queen_Diamonds.jpg"));
        sortedDeck.add(new Card("Queen_Hearts", 10, "Cards/Queen_Hearts.jpg"));
        sortedDeck.add(new Card("Queen_Spades", 10, "Cards/Queen_Spades.jpg"));

        sortedDeck.add(new Card("King_Clubs", 10, "Cards/King_Clubs.jpg"));
        sortedDeck.add(new Card("King_Diamonds", 10, "Cards/King_Diamonds.jpg"));
        sortedDeck.add(new Card("King_Hearts", 10, "Cards/King_Hearts.jpg"));
        sortedDeck.add(new Card("King_Spades", 10, "Cards/King_Spades.jpg"));

        sortedDeck.add(new Card("Ace_Clubs", 11, "Cards/Ace_Clubs.jpg"));
        sortedDeck.add(new Card("Ace_Diamonds", 11, "Cards/Ace_Diamonds.jpg"));
        sortedDeck.add(new Card("Ace_Hearts", 11, "Cards/Ace_Hearts.jpg"));
        sortedDeck.add(new Card("Ace_Spades", 11, "Cards/Ace_Spades.jpg"));

        Collections.shuffle(sortedDeck);

        deck = new Stack<>();

        for (int index = 0; index < 51; index++) {
            deck.push(sortedDeck.get(index));
        }

    }

    public Stack<Card> getDeck() {
        return deck;
    }
}
