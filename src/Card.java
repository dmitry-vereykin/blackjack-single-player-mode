/**
 * Created by Dmitry Vereykin on 12/15/2015.
 */
public class Card {
    public String nameOfCard;
    public String nameOfFile;
    public int valueOfCard;

    public Card(String nameOfCard, int valueOfCard, String nameOfFile) {
        this.nameOfCard = nameOfCard;
        this.valueOfCard = valueOfCard;
        this.nameOfFile = nameOfFile;
    }

    public void setValueOfCard(int valueOfCard) {
        this.valueOfCard = valueOfCard;
    }

    public String getNameOfCard() {
        return nameOfCard;
    }

    public String getNameOfFile() {
        return nameOfFile;
    }

    public int getValueOfCard() {
        return valueOfCard;
    }
}
