/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class GamesWon extends JPanel {

    public JLabel dealerLabelGames;
    public JLabel playerLabelGames;
    public JLabel dealerGamesWin;
    public JLabel playerGamesWin;


    public GamesWon() {
        setLayout(new GridLayout(2, 2));

        dealerLabelGames = new JLabel(" Dealer:  ");
        dealerGamesWin = new JLabel("0");
        playerLabelGames = new JLabel(" Player:  ");
        playerGamesWin = new JLabel("0");

        add(dealerLabelGames);
        add(dealerGamesWin);
        add(playerLabelGames);
        add(playerGamesWin);

        setBorder(BorderFactory.createTitledBorder("Games Won"));

    }

}
