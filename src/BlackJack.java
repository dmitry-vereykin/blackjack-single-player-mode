import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by Dmitry Vereykin on 12/15/2015.
 */
public class BlackJack {
    ArrayList<Card> dealersHand;
    ArrayList<Card> playersHand;
    Deck deck;
    Stack<Card> newDeck;

    public BlackJack() {
        dealersHand = new ArrayList<>();
        playersHand = new ArrayList<>();
        dealersHand.clear();
        playersHand.clear();
        deck = new Deck();
        newDeck = deck.getDeck();
    }

    public void firstDeal() {
        playersHand.add(newDeck.pop());
        dealersHand.add(newDeck.pop());
        playersHand.add(newDeck.pop());
        dealersHand.add(newDeck.pop());
    }

    public void dealForPlayer() {
        playersHand.add(newDeck.pop());
    }

    public void dealForDealer() {
        dealersHand.add(newDeck.pop());
    }

    public int recalculatePlayersCards() {
        int value = 0;

        for (int index = 0; index < playersHand.size(); index++) {
            value = value + playersHand.get(index).getValueOfCard();
        }

      return value;
    }

    public int recalculateDealersCards() {
        int value = 0;

        for (int index = 0; index < dealersHand.size(); index++) {
            value = value + dealersHand.get(index).getValueOfCard();
        }

        return value;
    }

}
