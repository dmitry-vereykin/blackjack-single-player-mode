/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class BlackJackGUI extends JFrame {
    private HandsPanel hands;
    private Points points;
    private GamesWon gamesWon;
    private TopBanner topBanner;
    private JPanel buttonPanel;

    private JButton play;
    private JButton hit;
    private JButton stand;

    public BlackJack blackJack;
    public ImageIcon redBackface = (new ImageIcon(getClass().getResource("Cards/Backface_Red.jpg")));
    public ImageIcon blueBackface = (new ImageIcon(getClass().getResource("Cards/Backface_Blue.jpg")));

    int playersMove;
    int dealersMove;

    int playerWon;
    int dealerWon;

    String player = "player";
    String dealer = "dealer";


    public BlackJackGUI() {
        setTitle("Black Jack Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        topBanner = new TopBanner();
        hands = new HandsPanel();
        points = new Points();
        gamesWon = new GamesWon();
        buttonPanel = new JPanel(new GridLayout(1, 4));

        play = new JButton("Play");
        hit = new JButton("Hit");
        stand = new JButton("Stand");

        play.addActionListener(new newGameButtonListener());
        hit.addActionListener(new HitButtonListener());
        stand.addActionListener(new StandButtonListener());

        for (int index = 0; index < 6; index++) {
            hands.buttonsForAces[index].addActionListener(new AcesButtonListener());
        }

        buttonPanel.add(play);
        buttonPanel.add(new JLabel("      "));
        buttonPanel.add(hit);
        buttonPanel.add(stand);

        add(topBanner, BorderLayout.NORTH);
        add(hands, BorderLayout.WEST);
        add(gamesWon, BorderLayout.EAST);
        add(points, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);

        blackJack = new BlackJack();

        playerWon = 0;
        dealerWon = 0;

        hit.setVisible(false);
        stand.setVisible(false);

        this.pack();
        this.setVisible(true);
        setLocationRelativeTo(null);
    }

    private class newGameButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == play) {

                newGame();

                blackJack.firstDeal();

                hit.setVisible(true);
                stand.setVisible(true);

                points.dealerPoints.setText("0");
                points.playerPoints.setText("0");

                //ImageIcon newCard = (new ImageIcon(getClass().getResource(blackJack.dealersHand.get(0).nameOfFile)));
                hands.dealersLabels[0].setIcon(redBackface);
                hands.dealersLabels[0].setVisible(true);

                ImageIcon newCard = (new ImageIcon(getClass().getResource(blackJack.playersHand.get(0).nameOfFile)));
                hands.playersLabels[0].setIcon(newCard);
                hands.playersLabels[0].setVisible(true);

                newCard = (new ImageIcon(getClass().getResource(blackJack.dealersHand.get(1).nameOfFile)));
                hands.dealersLabels[1].setIcon(newCard);
                hands.dealersLabels[1].setVisible(true);

                newCard = (new ImageIcon(getClass().getResource(blackJack.playersHand.get(1).nameOfFile)));
                hands.playersLabels[1].setIcon(newCard);
                hands.playersLabels[1].setVisible(true);

                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                points.dealerPoints.setText(String.valueOf((blackJack.recalculateDealersCards()) - blackJack.dealersHand.get(0).getValueOfCard()));


                checkIfPlayerHasAces(0);
                checkIfPlayerHasAces(1);
                checkIfPlayerHas21();

                playersMove = 0;
                dealersMove = 0;
            }
        }
    }

    private class HitButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == hit) {
                playersMove++;
            }

            blackJack.dealForPlayer();

            ImageIcon newCard = (new ImageIcon(getClass().getResource(blackJack.playersHand.get(1 + playersMove).nameOfFile)));
            hands.playersLabels[1 + playersMove].setIcon(newCard);
            hands.playersLabels[1 + playersMove].setVisible(true);

            points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));

            if (!checkIfPlayerHasAces(1 + playersMove)) {
                checkForBust(player);
            }

            checkIfPlayerHas21();

        }
    }

    private class StandButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            ImageIcon newCard = (new ImageIcon(getClass().getResource(blackJack.dealersHand.get(0).nameOfFile)));
            hands.dealersLabels[0].setIcon(newCard);
            hands.dealersLabels[0].setVisible(true);

            points.dealerPoints.setText(String.valueOf(blackJack.recalculateDealersCards()));

            hit.setVisible(false);
            stand.setVisible(false);

            for (int index = 0; index < blackJack.dealersHand.size(); index++) {
                if (blackJack.dealersHand.get(index).getValueOfCard() == 11 && blackJack.recalculateDealersCards() > 11) {
                    blackJack.dealersHand.get(index).setValueOfCard(1);
                    points.dealerPoints.setText(String.valueOf(blackJack.recalculateDealersCards()));
                }
            }


            while (true) {

                if (blackJack.recalculateDealersCards() < 17) {

                    blackJack.dealForDealer();

                    for (int index = 0; index < blackJack.dealersHand.size(); index++) {
                        if (blackJack.dealersHand.get(index).getValueOfCard() == 11 && blackJack.recalculateDealersCards() > 11) {
                            blackJack.dealersHand.get(index).setValueOfCard(1);
                            points.dealerPoints.setText(String.valueOf(blackJack.recalculateDealersCards()));
                        }
                    }

                    newCard = (new ImageIcon(getClass().getResource(blackJack.dealersHand.get(2 + dealersMove).nameOfFile)));
                    hands.dealersLabels[2 + dealersMove].setIcon(newCard);
                    hands.dealersLabels[2 + dealersMove].setVisible(true);

                    points.dealerPoints.setText(String.valueOf(blackJack.recalculateDealersCards()));


                    checkForBust(dealer);
                    dealersMove++;

                } else {
                    break;
                }

            }

            if (!checkWhoWon()) {
                checkIfBothHaveSame();
            }

            }

        }

    private class AcesButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == hands.buttonsForAces[0] && blackJack.playersHand.get(0).getValueOfCard() == 11) {
                blackJack.playersHand.get(0).setValueOfCard(1);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[0].setText("1");
            } else if (e.getSource() == hands.buttonsForAces[0] && blackJack.playersHand.get(0).getValueOfCard() == 1) {
                blackJack.playersHand.get(0).setValueOfCard(11);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[0].setText("11");
            }

            if (e.getSource() == hands.buttonsForAces[1] && blackJack.playersHand.get(1).getValueOfCard() == 11) {
                blackJack.playersHand.get(1).setValueOfCard(1);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[1].setText("1");
            } else if (e.getSource() == hands.buttonsForAces[1] && blackJack.playersHand.get(1).getValueOfCard() == 1) {
                blackJack.playersHand.get(1).setValueOfCard(11);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[1].setText("11");
            }

            if (e.getSource() == hands.buttonsForAces[2] && blackJack.playersHand.get(2).getValueOfCard() == 11) {
                blackJack.playersHand.get(2).setValueOfCard(1);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[2].setText("1");
            } else if (e.getSource() == hands.buttonsForAces[2] && blackJack.playersHand.get(2).getValueOfCard() == 1) {
                blackJack.playersHand.get(2).setValueOfCard(11);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[2].setText("11");
            }

            if (e.getSource() == hands.buttonsForAces[3] && blackJack.playersHand.get(3).getValueOfCard() == 11) {
                blackJack.playersHand.get(3).setValueOfCard(1);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[3].setText("1");
            } else if (e.getSource() == hands.buttonsForAces[3] && blackJack.playersHand.get(3).getValueOfCard() == 1) {
                blackJack.playersHand.get(3).setValueOfCard(11);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[3].setText("11");
            }

            if (e.getSource() == hands.buttonsForAces[4] && blackJack.playersHand.get(4).getValueOfCard() == 11) {
                blackJack.playersHand.get(4).setValueOfCard(1);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[4].setText("1");
            } else if (e.getSource() == hands.buttonsForAces[4] && blackJack.playersHand.get(4).getValueOfCard() == 1) {
                blackJack.playersHand.get(4).setValueOfCard(11);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[4].setText("11");
            }

            if (e.getSource() == hands.buttonsForAces[5] && blackJack.playersHand.get(5).getValueOfCard() == 11) {
                blackJack.playersHand.get(5).setValueOfCard(1);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[5].setText("1");
            } else if (e.getSource() == hands.buttonsForAces[5] && blackJack.playersHand.get(5).getValueOfCard() == 1) {
                blackJack.playersHand.get(5).setValueOfCard(11);
                points.playerPoints.setText(String.valueOf(blackJack.recalculatePlayersCards()));
                hands.buttonsForAces[5].setText("11");
            }


        }
    }

    public boolean checkIfPlayerHas21() {
        if (blackJack.recalculatePlayersCards() == 21) {
            playerWon++;
            gamesWon.playerGamesWin.setText(String.valueOf(playerWon));
            JOptionPane.showMessageDialog(null, "Blackjack! You won!");
            newGame();
            return true;
        } else {
            return false;
        }
    }

    public boolean checkIfBothHaveSame() {
        if (blackJack.recalculatePlayersCards() == blackJack.recalculateDealersCards()) {
            JOptionPane.showMessageDialog(null, "Draw!");
            newGame();
            return true;
        } else {
            return false;
        }
    }

    public boolean checkForBust(String who) {
        if (who.equals("player")) {
            if (blackJack.recalculatePlayersCards() > 21) {
                dealerWon++;
                gamesWon.dealerGamesWin.setText(String.valueOf(dealerWon));
                JOptionPane.showMessageDialog(null, "Bust!");
                newGame();
                return true;
            }
        } else if (who.equals("dealer")) {
            if (blackJack.recalculateDealersCards() > 21) {
                playerWon++;
                gamesWon.playerGamesWin.setText(String.valueOf(playerWon));
                JOptionPane.showMessageDialog(null, "Dealer busted!");
                newGame();
                return true;
            }
        }
        return false;
    }


    public boolean checkWhoWon(){
        if ((blackJack.recalculateDealersCards() > blackJack.recalculatePlayersCards()) && (blackJack.recalculateDealersCards() <= 21)) {
            dealerWon++;
            gamesWon.dealerGamesWin.setText(String.valueOf(dealerWon));
            JOptionPane.showMessageDialog(null, "Dealer won!");
            newGame();
            return true;
        } else if ((blackJack.recalculatePlayersCards() > blackJack.recalculateDealersCards()) && (blackJack.recalculatePlayersCards() <= 21)) {
            playerWon++;
            gamesWon.playerGamesWin.setText(String.valueOf(playerWon));
            JOptionPane.showMessageDialog(null, "You won!");
            newGame();
            return true;
        } else {
            return false;
        }
    }

    public void newGame() {

        dealersMove = 0;
        playersMove = 0;

        blackJack.playersHand.clear();
        blackJack.dealersHand.clear();

        points.dealerPoints.setText("0");
        points.playerPoints.setText("0");

        hit.setVisible(false);
        stand.setVisible(false);

        for (int index = 0; index < 6; index++) {
            hands.dealersLabels[index].setIcon(blueBackface);
            hands.dealersLabels[index].setVisible(false);
        }

        for (int index = 0; index < 6; index++) {
            hands.playersLabels[index].setIcon(blueBackface);
            hands.playersLabels[index].setVisible(false);
        }

        for (int index = 0; index < 6; index++) {
            hands.buttonsForAces[index].setVisible(false);
        }

        if (blackJack.newDeck.size() < 4) {
            int result = JOptionPane.showConfirmDialog(null, "Close the game?", "No more cards!", JOptionPane.OK_OPTION);
            if (result == 0) System.exit(0);
        }

    }

    public boolean checkIfPlayerHasAces(int index) {
            if (blackJack.playersHand.get(index).getValueOfCard() == 11) {
                hands.buttonsForAces[index].setText("11");
                hands.buttonsForAces[index].setVisible(true);
                return true;
            }
        return false;
    }

    public static void main(String[] args) {
        new BlackJackGUI();
    }
}